﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Decorator
{
	class ACheese : Aggregates
	{
		public ACheese(Sandwiches sandwich) : base(sandwich)
		{
			aggregate = "cheese";
			cost = 15.00;
		}
	}
}
