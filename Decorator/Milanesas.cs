﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Decorator
{
	class Milanesas : Sandwiches
	{
		public override string getName()
		{
			return "Milanesa Sandwich";
		}
		public override double getPrice()
		{
			return 50.00;
		}
	}
}
