﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Decorator
{
	class AChips : Aggregates
	{
		public AChips(Sandwiches sandwich) : base(sandwich)
		{
			aggregate = "Chips";
			cost = 20.00;
		}
	}
}
