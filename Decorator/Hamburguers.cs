﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Decorator
{
	class Hamburguers : Sandwiches
	{
		public override string getName()
		{
			return "Hamburguer";
		}
		public override double getPrice()
		{
			return 40.00;
		}
	}
}
