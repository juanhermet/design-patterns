﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Decorator
{
	class Program
	{
		static void Main(string[] args)
		{
			Sandwiches newSandwich = new Milanesas();
			Console.WriteLine("I order a Milanesa sandwich");
			Console.WriteLine($"your order: {newSandwich.getName()}, price: {newSandwich.getPrice()}");
			Console.WriteLine("..............................................");
			Console.ReadKey();
			Console.WriteLine("but, I wish aggregate chips to my sandwich. Then I order chips");
			newSandwich = new AChips(newSandwich);
			Console.WriteLine($"your order: {newSandwich.getName()}, price: {newSandwich.getPrice()}");
			Console.WriteLine("..............................................");
			Console.ReadKey();
			Console.WriteLine("but, I wish aggregate cheese to my sandwich:");
			newSandwich = new ACheese(newSandwich);
			Console.WriteLine($"your order: {newSandwich.getName()}, price: {newSandwich.getPrice()}");
			Console.WriteLine("..............................................");
			Console.ReadKey();

		}
	}
}
