﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Decorator
{
	abstract class Sandwiches
	{
		public Sandwiches() { }
		public abstract string getName();
		public abstract double getPrice();
	}
}
