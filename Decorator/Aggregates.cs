﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Decorator
{
	class Aggregates : Sandwiches
	{
		protected Sandwiches sandwich;
		protected string aggregate;
		protected double cost;
		public Aggregates(Sandwiches sandwich) : base()
		{
			this.sandwich = sandwich;
		}

		public override string getName()
		{
			return $"{sandwich.getName()} with {aggregate}";
		}

		public override double getPrice()
		{
			return sandwich.getPrice() + cost;
		}
	}
}
