﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DesignPatterns
{
	interface IFlyweight
	{
		//I create only 2 methods because they're the extrinsic behaviour.
		//what principle is being considered?
		string getDni();
		void show();
	}
}
