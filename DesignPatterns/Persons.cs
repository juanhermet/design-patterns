﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DesignPatterns
{
	class Persons : IFlyweight
	{
		string name;
		string dni;
		DateTime date;

		public Persons(string name, string dni, DateTime date)
		{
			this.name = name;
			this.dni = dni;
			this.date = date;
		}

		public string Name { get => name; set => name = value; }
		public string Dni { get => dni; set => dni = value; }
		public DateTime Date { get => date; set => date = value; }


		public string getDni()
		{
			return Dni;
		}

		public void show()
		{
			Console.WriteLine($"Name: {Name}");
			Console.WriteLine($"Date: {Date}");
			Console.WriteLine($"Dni: {Dni}");
		}
	}
}
