﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DesignPatterns
{
	class FlyweightFactory
	{
		List<IFlyweight> ListFlyweight = new List<IFlyweight>();

		public int Add(IFlyweight person)
		{
			int i = 0;
			bool found = false;
			while((i < ListFlyweight.Count()) || (found))
			{
				if(ListFlyweight[i].getDni() == person.getDni())
				{
					found = true;
				}
				i++;
			}

			if (found)
			{
				Console.WriteLine("The Object is added.");
				i = -1;
			}
			else
			{
				ListFlyweight.Add(person);
				i = ListFlyweight.Count - 1;
			}

			return i;
		}

		public IFlyweight this[int index]
		{
			get { return ListFlyweight[index]; }
		}
	}
}
