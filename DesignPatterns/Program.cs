﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DesignPatterns
{
	class Program
	{
		static void Main(string[] args)
		{
			int i;
			List<int> Profesors = new List<int>();
			List<int> Students = new List<int>();

			FlyweightFactory factory = new FlyweightFactory();
			IFlyweight newPerson1 = new Persons("juan", "22587453", new DateTime(1990, 2, 27));
			i=factory.Add(newPerson1);
			Profesors.Add(i);
			Students.Add(i);

			IFlyweight newPerson2 = new Persons("José", "96785413", new DateTime(1980, 12, 14));
			i =factory.Add(newPerson2);
			Profesors.Add(i);

			Console.WriteLine("I show all profesors:");
			foreach(int x in Profesors)
			{
				Persons person = (Persons)factory[x];
				person.show();
				Console.WriteLine("-----------------------------");
			}
		
			Console.WriteLine("Now, I show all students:");

			foreach(int x in Students)
			{
				Persons person = (Persons)factory[x];
				person.show();
				Console.WriteLine("-----------------------------");
			}
			Console.WriteLine("Now, I add a person who is in de List:");
			IFlyweight person1 = new Persons("juan", "22587453", new DateTime(1990, 2, 27));
			Console.WriteLine("-----------------------------");
			Console.ReadKey();
		}
	}
}
