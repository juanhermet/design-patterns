﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Prototype
{
	interface ICharacter
	{
		Characters clone(string name);
		void show();

	}
}
