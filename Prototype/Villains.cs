﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Prototype
{
	class Villains : Characters
	{
		public Villains()
		{

		}
		public Villains(string name,string power,string reason) : base(name,power,reason)
		{

		}
		public override Characters clone(string name)
		{
			Villains villain = new Villains(name,"Darkness","Evil");
			return villain;
		}
	}
}
