﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Prototype
{
	class Heroes : Characters
	{
		public Heroes()
		{

		}
		public Heroes(string name,string power,string reason) : base(name,power,reason)
		{

		}
		public override Characters clone(string name)
		{
			Heroes hero = new Heroes(name,"Light","Justice");
			return hero;
		}
	}
}
