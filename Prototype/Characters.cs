﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Prototype
{
	abstract class Characters : ICharacter
	{
		protected string name;
		protected string power;
		protected string reason;

		protected Characters()
		{

		}
		protected Characters(string name, string power, string reason)
		{
			this.name = name;
			this.power = power;
			this.reason = reason;
		}

		public void show()
		{
			Console.WriteLine($"Name: {name}");
			Console.WriteLine($"Power: {power}");
			Console.WriteLine($"Reason: {reason}");
		}
		public abstract Characters clone(string name);
	}
}
