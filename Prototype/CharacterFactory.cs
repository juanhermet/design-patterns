﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Prototype
{
	class CharacterFactory
	{
		
		public void ChooseCharacter()
		{
			ICharacter character;
			Characters characters;
			string answer;
			string name;
			Console.WriteLine($"What's your name?");
			name = Console.ReadLine();
			Console.WriteLine($"choose if you're a Hero or Villain:");
			answer = Console.ReadLine();
			switch (answer)
			{
				case "Hero":
				case "Heroes":
				case "hero":
				case "heroes":
					characters = new Heroes();
					break;
				case "Villain":
				case "Villains":
				case "villain":
				case "villains":
					characters = new Villains();
					break;
				default:
					characters = new Heroes();
					break;
			}
			character = characters.clone(name);
			character.show();
			Console.ReadKey();
		}
	}
}
