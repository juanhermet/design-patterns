﻿namespace Singleton
{
	partial class Form1
	{
		/// <summary>
		/// Required designer variable.
		/// </summary>
		private System.ComponentModel.IContainer components = null;

		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		/// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
		protected override void Dispose(bool disposing)
		{
			if (disposing && (components != null))
			{
				components.Dispose();
			}
			base.Dispose(disposing);
		}

		#region Windows Form Designer generated code

		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			this.lblWelcome = new System.Windows.Forms.Label();
			this.lblLegName = new System.Windows.Forms.Label();
			this.lblLegLstNm = new System.Windows.Forms.Label();
			this.lblLegUser = new System.Windows.Forms.Label();
			this.lblLegGender = new System.Windows.Forms.Label();
			this.lblName = new System.Windows.Forms.Label();
			this.lblLastName = new System.Windows.Forms.Label();
			this.lblUser = new System.Windows.Forms.Label();
			this.lblGender = new System.Windows.Forms.Label();
			this.SuspendLayout();
			// 
			// lblWelcome
			// 
			this.lblWelcome.AutoSize = true;
			this.lblWelcome.Location = new System.Drawing.Point(44, 36);
			this.lblWelcome.Name = "lblWelcome";
			this.lblWelcome.Size = new System.Drawing.Size(35, 13);
			this.lblWelcome.TabIndex = 0;
			this.lblWelcome.Text = "label1";
			// 
			// lblLegName
			// 
			this.lblLegName.AutoSize = true;
			this.lblLegName.Location = new System.Drawing.Point(44, 93);
			this.lblLegName.Name = "lblLegName";
			this.lblLegName.Size = new System.Drawing.Size(38, 13);
			this.lblLegName.TabIndex = 1;
			this.lblLegName.Text = "Name:";
			// 
			// lblLegLstNm
			// 
			this.lblLegLstNm.AutoSize = true;
			this.lblLegLstNm.Location = new System.Drawing.Point(44, 127);
			this.lblLegLstNm.Name = "lblLegLstNm";
			this.lblLegLstNm.Size = new System.Drawing.Size(61, 13);
			this.lblLegLstNm.TabIndex = 2;
			this.lblLegLstNm.Text = "Last Name:";
			// 
			// lblLegUser
			// 
			this.lblLegUser.AutoSize = true;
			this.lblLegUser.Location = new System.Drawing.Point(44, 156);
			this.lblLegUser.Name = "lblLegUser";
			this.lblLegUser.Size = new System.Drawing.Size(32, 13);
			this.lblLegUser.TabIndex = 3;
			this.lblLegUser.Text = "User:";
			// 
			// lblLegGender
			// 
			this.lblLegGender.AutoSize = true;
			this.lblLegGender.Location = new System.Drawing.Point(44, 188);
			this.lblLegGender.Name = "lblLegGender";
			this.lblLegGender.Size = new System.Drawing.Size(45, 13);
			this.lblLegGender.TabIndex = 4;
			this.lblLegGender.Text = "Gender:";
			// 
			// lblName
			// 
			this.lblName.AutoSize = true;
			this.lblName.Location = new System.Drawing.Point(143, 93);
			this.lblName.Name = "lblName";
			this.lblName.Size = new System.Drawing.Size(29, 13);
			this.lblName.TabIndex = 5;
			this.lblName.Text = "label";
			// 
			// lblLastName
			// 
			this.lblLastName.AutoSize = true;
			this.lblLastName.Location = new System.Drawing.Point(143, 127);
			this.lblLastName.Name = "lblLastName";
			this.lblLastName.Size = new System.Drawing.Size(29, 13);
			this.lblLastName.TabIndex = 6;
			this.lblLastName.Text = "label";
			// 
			// lblUser
			// 
			this.lblUser.AutoSize = true;
			this.lblUser.Location = new System.Drawing.Point(143, 156);
			this.lblUser.Name = "lblUser";
			this.lblUser.Size = new System.Drawing.Size(29, 13);
			this.lblUser.TabIndex = 7;
			this.lblUser.Text = "label";
			// 
			// lblGender
			// 
			this.lblGender.AutoSize = true;
			this.lblGender.Location = new System.Drawing.Point(143, 188);
			this.lblGender.Name = "lblGender";
			this.lblGender.Size = new System.Drawing.Size(29, 13);
			this.lblGender.TabIndex = 8;
			this.lblGender.Text = "label";
			// 
			// Form1
			// 
			this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
			this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
			this.ClientSize = new System.Drawing.Size(409, 450);
			this.Controls.Add(this.lblGender);
			this.Controls.Add(this.lblUser);
			this.Controls.Add(this.lblLastName);
			this.Controls.Add(this.lblName);
			this.Controls.Add(this.lblLegGender);
			this.Controls.Add(this.lblLegUser);
			this.Controls.Add(this.lblLegLstNm);
			this.Controls.Add(this.lblLegName);
			this.Controls.Add(this.lblWelcome);
			this.Name = "Form1";
			this.Text = "Form1";
			this.Load += new System.EventHandler(this.Form1_Load);
			this.ResumeLayout(false);
			this.PerformLayout();

		}

		#endregion

		private System.Windows.Forms.Label lblWelcome;
		private System.Windows.Forms.Label lblLegName;
		private System.Windows.Forms.Label lblLegLstNm;
		private System.Windows.Forms.Label lblLegUser;
		private System.Windows.Forms.Label lblLegGender;
		private System.Windows.Forms.Label lblName;
		private System.Windows.Forms.Label lblLastName;
		private System.Windows.Forms.Label lblUser;
		private System.Windows.Forms.Label lblGender;
	}
}