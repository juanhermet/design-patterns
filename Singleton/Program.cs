﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data.SqlClient;
namespace Singleton
{
	class Program
	{
		static void Main(string[] args)
		{
			//how we don't use databases, I create some Users.
			IUsers user1 = new Users("juan", "Fuensalida", "juan123", "1234", "Male");
			IUsers user2 = new Users("Valentina", "Fuensalida", "vale123", "1234", "Female");
			
			string puser;
			string ppass;
			
			Console.WriteLine("Please, insert your user:");
			puser = Console.ReadLine().ToString();
			Console.WriteLine("Please, insert your password:");
			ppass = Console.ReadLine().ToString();
			//consult if user and password belong to some user. how I have 2 it's fast and easy.
			if (user1.probeUser(puser) & user1.probePass(ppass))
			{
				SingletonUser.Instance(user1);
			}
			else
			{
				if(user2.probeUser(puser) & user2.probePass(ppass))
				{
					SingletonUser.Instance(user2);
				}
			}
			
			if(SingletonUser.GetInstance() != null)
			{
				Form1 newForm = new Form1();
				newForm.ShowDialog();
			}
			else
			{
				Console.WriteLine("the user or password is wrong.");
			}

			Console.ReadKey();
		}
	}
}
