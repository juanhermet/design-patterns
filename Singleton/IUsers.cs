﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Singleton
{
	interface IUsers
	{
		void Show();
		string WriteWelcome();
		bool probeUser(string user);
		bool probePass(string pass);
		string getName();
		string getLastName();
		string getUser();
		string getPass();
		string getGender();
	}
}
