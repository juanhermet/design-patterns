﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Singleton
{
	public partial class Form1 : Form
	{
		public Form1()
		{
			InitializeComponent();
		}

		private void Form1_Load(object sender, EventArgs e)
		{
			if(SingletonUser.GetInstance() != null)
			{
				//here, I probe the use of singleton. it has still the parameters that we putted in the logger.
				IUsers user = (Users)SingletonUser.GetInstance();
				lblWelcome.Text = user.WriteWelcome();
				lblName.Text = user.getName();
				lblLastName.Text = user.getLastName();
				lblUser.Text = user.getUser();
				lblGender.Text = user.getGender();
			}
		}
	}
}
