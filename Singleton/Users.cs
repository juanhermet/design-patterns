﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Singleton
{
	class Users : IUsers
	{
		string name;
		string lastname;
		string user;
		string pass;
		string gender;
		public Users(string name,string lastname, string user, string pass,string gene)
		{
			this.name = name;
			this.lastname = lastname;
			this.user = user;
			this.pass = pass;
			this.gender = gene;
		}

		public string getName()
		{
			return name;
		}
		public string getLastName()
		{
			return lastname;
		}
		public string getUser()
		{
			return user;
		}
		public string getPass()
		{
			return pass;
		}
		public string getGender()
		{
			return gender;
		}
		public void Show()
		{
			Console.WriteLine($"Name: {name}");
			Console.WriteLine($"Last name: {lastname}");
			Console.WriteLine($"Gene: {gender}");
		}

		public string WriteWelcome()
		{
			string result;
			if(gender == "Male")
			{
				result = $"Welcome, mister {name}, What do you want?";
			}
			else
			{
				if(gender == "Female")
				{
					result = $"Welcome, miss {name}, What do you want?";
				}
				else
				{
					result = $"Welcome {name}, What do you want?";
				}
			}
			
			return result;
		}

		public bool probeUser(string user)
		{
			if(this.user == user)
			{
				return true;
			}
			else
			{
				return false;
			}
		}

		public bool probePass(string pass)
		{
			if(this.pass == pass)
			{
				return true;
			}
			else
			{
				return false;
			}
		}
	}
}
