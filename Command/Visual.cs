﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Command
{
	public partial class Visual : Form
	{
		
		public Visual()
		{
			InitializeComponent();
		}

		private void button1_Click(object sender, EventArgs e)
		{
			ICommand LOn = new TurnOnCommand();
			ICommand LOff = new TurnOffCommand();
			Switch swtch = new Switch(LOn, LOff);
			if (button1.Text == "OFF")
			{
				txtLight.BackColor = swtch.TurnOn();
				button1.Text = "ON";
			}
			else
			{
				txtLight.BackColor = swtch.TurnOff();
				button1.Text = "OFF";
			}
		}

		private void Visual_Load(object sender, EventArgs e)
		{

		}
	}
}
