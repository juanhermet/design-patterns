﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Drawing;
namespace Command
{
	class TurnOffCommand : ICommand
	{
		Lights light;

		public TurnOffCommand()
		{
			light = new Lights(Color.Black);
		}

		public Color execute()
		{
			return light.State;
		}

	}
}
