﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Drawing;

namespace Command
{
	class TurnOnCommand : ICommand
	{
		Lights light;

		public TurnOnCommand()
		{
			light = new Lights(Color.Blue);
		}

		public Color execute()
		{
			return light.State;
		}
	}
}
