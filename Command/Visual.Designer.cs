﻿namespace Command
{
	partial class Visual
	{
		/// <summary>
		/// Required designer variable.
		/// </summary>
		private System.ComponentModel.IContainer components = null;

		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		/// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
		protected override void Dispose(bool disposing)
		{
			if (disposing && (components != null))
			{
				components.Dispose();
			}
			base.Dispose(disposing);
		}

		#region Windows Form Designer generated code

		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			this.button1 = new System.Windows.Forms.Button();
			this.txtLight = new System.Windows.Forms.TextBox();
			this.SuspendLayout();
			// 
			// button1
			// 
			this.button1.Location = new System.Drawing.Point(244, 345);
			this.button1.Name = "button1";
			this.button1.Size = new System.Drawing.Size(154, 47);
			this.button1.TabIndex = 0;
			this.button1.Text = "CLICK ME!";
			this.button1.UseVisualStyleBackColor = true;
			this.button1.Click += new System.EventHandler(this.button1_Click);
			// 
			// txtLight
			// 
			this.txtLight.BackColor = System.Drawing.SystemColors.HotTrack;
			this.txtLight.Location = new System.Drawing.Point(244, 73);
			this.txtLight.Name = "txtLight";
			this.txtLight.Size = new System.Drawing.Size(154, 20);
			this.txtLight.TabIndex = 1;
			// 
			// Visual
			// 
			this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
			this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
			this.ClientSize = new System.Drawing.Size(682, 570);
			this.Controls.Add(this.txtLight);
			this.Controls.Add(this.button1);
			this.Name = "Visual";
			this.Text = "Command";
			this.Load += new System.EventHandler(this.Visual_Load);
			this.ResumeLayout(false);
			this.PerformLayout();

		}

		#endregion

		private System.Windows.Forms.Button button1;
		private System.Windows.Forms.TextBox txtLight;
	}
}