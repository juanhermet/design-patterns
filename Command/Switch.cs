﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Drawing;
namespace Command
{
	class Switch
	{
		ICommand turnOn;
		ICommand turnOff;
		public Switch(ICommand turnOn, ICommand turnOff)
		{
			this.turnOn = turnOn;
			this.turnOff = turnOff;
		}
		public Color TurnOn()
		{
			return turnOn.execute();
		}
		public Color TurnOff()
		{
			return turnOff.execute();
		}
	}
}
