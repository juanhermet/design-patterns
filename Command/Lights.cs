﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Drawing;
namespace Command
{
	class Lights // receiver
	{
		Color state;

		public Lights(Color state)
		{
			this.state = state;
		}

		public Color State { get => state; set => state = value; }
	}
}
