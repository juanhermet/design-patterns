﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Composite
{
	class Logarithm : Function
	{
		public Logarithm(string name): base(name)
		{
			this.name = name;
			function = "log(x)";
		}
		public override string Func()
		{
			return function;
		}
		public override string Name()
		{
			return name;
		}
		public void setAlgorithm(string x)
		{
			function = $"log({x})";
		}
	}
}
