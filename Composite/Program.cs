﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Composite
{
	class Program
	{
		static void Main(string[] args)
		{
			string first;
			string second;
			string anwser;
			Console.WriteLine("Choose a name for your function.");
			first = Console.ReadLine();
			Console.WriteLine("Choose a name for your another function.");
			second = Console.ReadLine();
			Consult log = new Logarithm(first);
			Consult sqr = new Squared(second);
			Consult op = new Operators((Logarithm)log,(Squared)sqr);
			do
			{
				op.call();
				Console.WriteLine("again?");
				anwser = Console.ReadLine();

			} while (anwser == "yes");
			Console.ReadKey();
		}
	}
}
