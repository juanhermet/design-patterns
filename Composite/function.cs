﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Composite
{
	abstract class Function: Consult 
	{
		protected string function;
		protected string name;

		public Function(string name)
		{
			this.name = name;
		}
		public virtual void call()
		{
			Console.WriteLine($"{Name()}(x) = {Func()}");
		}
		public abstract string Name();
		public abstract string Func();
	}
}
