﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Composite
{
	class Operators : Consult
	{
		Logarithm log;
		Squared squa;

		public Operators(Logarithm log, Squared squa)
		{
			this.log = log;
			this.squa = squa;
		}

		public void Add()
		{
			Console.WriteLine($"{log.Name()}(x) + {squa.Name()}(x) = {log.Func()} + {squa.Func()}");
		}
		public void Mult()
		{
			Console.WriteLine($"{log.Name()}(x) * {squa.Name()}(x) = {log.Func()} * {squa.Func()}");
		}
		public void Stract()
		{
			Console.WriteLine($"{log.Name()}(x) - {squa.Name()}(x) = {log.Func()} - {squa.Func()}");
		}
		public void Div()
		{
			Console.WriteLine($"{log.Name()}(x) / {squa.Name()}(x) = {log.Func()} / {squa.Func()}");
		}
		public void call()
		{
			int i;
			Console.WriteLine("hi, what do you do?\n 1-Sum\n 2-stract\n 3-Mult\n 4-Div");
			i = int.Parse(Console.ReadLine());
			if(i == 1)
			{
				Add();
			}
			if (i == 2)
			{
				Stract();
			}
			if(i == 3)
			{
				Mult();
			}
			if(i == 4)
			{
				Div();
			}
		}
		public void comp()
		{
			
			Console.WriteLine($"{log.Name()}({squa.Name()}) = {log.Func()} + {squa.Func()}");
		}
	}
}
