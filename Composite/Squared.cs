﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Composite
{
	class Squared : Function
	{
		public Squared(string name) : base(name)
		{
			this.name = name;
			function = $"x2";
		}

		public override string Func()
		{
			return function;
		}
		public override string Name()
		{
			return name;
		}
		public void setSquared(string x)
		{
			function = $"{x}2";
		}
	}
}
