﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Memento
{
	interface IPlayers
	{
		void restartStadistic(Stadistics stadistics);
		void addStadistics();
		Stadistics getStadistics();
		void rest();
		void Show();
		string getName();
		int getSpeed();
		int getAttack();
		int getLife();
		void receiveDamage(int damage);
		void setStadistics();
		void addWin();
	}
}
