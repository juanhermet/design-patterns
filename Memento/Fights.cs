﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Memento
{
	class Fights
	{
		IPlayers Player1;
		IPlayers Player2;
		bool win = false;
		public Fights(IPlayers player1, IPlayers player2)
		{
			this.Player1 = player1;
			this.Player2 = player2;
		}

		public void FightPlayers()
		{
			Console.WriteLine($"Start the Fight: {Player1.getName()} against {Player2.getName()}");
			Console.WriteLine($"{Player1.getName()}'s speed is {Player1.getSpeed()} and {Player2.getName()} is {Player2.getSpeed()}");
			Console.ReadKey();
			if (Player1.getSpeed() > Player2.getSpeed())
			{
				Console.WriteLine($"then, {Player1.getName()} hit first.");
				Console.ReadKey();
				do
				{
					if(Player1.getLife() > 0)
					{
						Console.WriteLine($"{Player1.getName()} hits");
						Player2.receiveDamage(Player1.getAttack());
						Console.ReadKey();
					}
					else
					{
						win = true;
					}
					if (Player2.getLife() > 0)
					{
						Console.WriteLine($"now, attack {Player2.getName()}");
						Player1.receiveDamage(Player2.getAttack());
						Console.ReadKey();
					}
					else
					{
						win = true;
					}
				} while(!win);
			}
			else
			{
				Console.WriteLine($"then, {Player2.getName()} hit first.");
				Console.ReadKey();
				do
				{
					if (Player2.getLife() > 0)
					{
						Console.WriteLine($"{Player2.getName()} hits");
						Player1.receiveDamage(Player2.getAttack());
						Console.ReadKey();
					}
					else
					{
						win = true;
					}
					if (Player1.getLife() > 0)
					{
						Console.WriteLine($"now, attack {Player1.getName()}");
						Player2.receiveDamage(Player1.getAttack());
						Console.ReadKey();
					}
					else
					{
						win = true;
					}
				} while (!win);
			}
			if(Player1.getLife() < 0)
			{
				Console.WriteLine($"{Player2.getName()} is the Winner!!!!");
				Player2.addWin();
				Console.WriteLine("sorry, but you lose :(");
			}
			else
			{
				Console.WriteLine($"{Player1.getName()} is the Winner!!!!");
				Player1.addWin();
				Console.WriteLine("good, you survive the game!");
			}
		}
	}
}
