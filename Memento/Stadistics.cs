﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Memento
{
	class Stadistics
	{
		//the Stadistics is the "Memento" that we want to save.
		int strength;
		int agility;
		int stamina;
		int wisdom;
		public Stadistics(int strength, int agility, int stamina,int wisdom)
		{
			this.strength = strength;
			this.agility = agility;
			this.stamina = stamina;
			this.wisdom = wisdom;
			
		}

		public int Strength { get => strength; set => strength = value; }
		public int Agility { get => agility; set => agility = value; }
		public int Stamina { get => stamina; set => stamina = value; }
		public int Wisdom { get => wisdom; set => wisdom = value; }

		public void Show()
		{
			Console.WriteLine($"Strength: {Strength}, Agility: {Agility}, Stamina: {Stamina}, Wisdom: {Wisdom}");
		}
		//I use a overload  to add the stadistics already loaded.
		public static Stadistics operator +(Stadistics stadistic1, Stadistics stadistic2)
		{
			Stadistics stadistic = new Stadistics(stadistic1.Strength + stadistic2.Strength, stadistic1.Agility + stadistic2.Agility, stadistic1.Stamina + stadistic2.Stamina, stadistic1.Wisdom + stadistic2.Wisdom);
			return stadistic;
		}
	}

	
}
