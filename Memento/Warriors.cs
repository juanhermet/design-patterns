﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Memento
{
	class Warriors : Players
	{

		public Warriors(string name) : base(name)
		{

		}

		public override void addStadistics()
		{
			life = life + stadistics.Stamina + stadistics.Strength;
			speed = speed + stadistics.Agility;
			scape = scape + stadistics.Agility;
			attack = attack + stadistics.Strength + 1;
			startLife = startLife + stadistics.Stamina + stadistics.Strength;
		}
		public override void receiveDamage(int damage)
		{
			int realDamage = damage - (stadistics.Agility);
			Random rnd = new Random();
			if (life > 0)
			{
				if (scape < rnd.Next(100))
				{
					life -= realDamage;
					Console.WriteLine($"{name} recived {realDamage} points of damage, Life: {life}");
				}
				else
				{
					Console.WriteLine($"{name} evaded the attack!");
					Console.WriteLine($"life: {life}");
				}
			}
		}
		public override void Show()
		{
			Console.WriteLine($"Player: {name}");
			Console.WriteLine($"Type: Warrior");
			Console.WriteLine($"Stadistics:");
			Console.WriteLine($"    Life: {life}");
			Console.WriteLine($"    Speed: {speed}");
			Console.WriteLine($"    Scape: {scape}");
			Console.WriteLine($"    Attack: {attack}");
			Console.WriteLine($"    Fights: {fights}");
			Console.WriteLine($"-------------------------------");

		}
	}
}
