﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Memento
{
	class Magicians : Players
	{
		public Magicians(string name) : base(name)
		{

		}

		public override void addStadistics()
		{
			life = life + stadistics.Stamina;
			speed = speed + stadistics.Agility+2;
			scape = scape + stadistics.Agility+3;
			attack = attack + stadistics.Wisdom;
			startLife = startLife + stadistics.Stamina;
		}
		public override void receiveDamage(int damage)
		{
			int realDamage = damage - (stadistics.Agility);
			Random rnd = new Random();
			if (life > 0)
			{
				if (scape < rnd.Next(100))
				{
					life -= realDamage;
					Console.WriteLine($"{name} recived {realDamage} points of damage, Life: {life}");
				}
				else
				{
					Console.WriteLine($"{name} evaded the attack!");
					Console.WriteLine($"life: {life}");
				}
			}
		}
		public override void Show()
		{
			Console.WriteLine($"Player: {name}");
			Console.WriteLine($"Type: Magician");
			Console.WriteLine($"Stadistics:");
			Console.WriteLine($"    Life: {life}");
			Console.WriteLine($"    Speed: {speed}");
			Console.WriteLine($"    Scape: {scape}");
			Console.WriteLine($"    Attack: {attack}");
			Console.WriteLine($"    Fights: {fights}");
			Console.WriteLine($"-------------------------------");
		}
	}
}
