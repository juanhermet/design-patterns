﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Memento
{
	class Program
	{
		static void Main(string[] args)
		{
			IPlayers me;
			CheckPoint checkPoint = new CheckPoint();
			string nme;
			int clas;
			Random rnd = new Random();
			int rndStr;
			int rndAgi;
			int rndStm;
			int rndWdm;
			Stadistics stadistic1;
			Console.WriteLine("welcome to the game of knights, what's your name?");
			nme = Console.ReadLine().ToString();
			Console.WriteLine($"ok {nme}, please, select a class:\n 1-Warrior\n 2-Magician\n 3-Hunter");
			clas = int.Parse(Console.ReadLine());
			switch (clas)
			{
				case 1:
					me = new Warriors(nme);
					rndStr = rnd.Next(5);
					rndAgi = rnd.Next(3);
					rndStm = rnd.Next(5);
					rndWdm = 0;
					Console.WriteLine($"So, you're a warrior. let´s go!");
					break;
				case 2:
					me = new Magicians(nme);
					rndStr = 0;
					rndAgi = rnd.Next(3);
					rndStm = rnd.Next(3);
					rndWdm = rnd.Next(5);
					Console.WriteLine($"So, you're a magician. let´s go!");
					break;
				case 3:
					me = new Hunters(nme);
					rndStr = rnd.Next(5);
					rndAgi = rnd.Next(3);
					rndStm = rnd.Next(3);
					rndWdm = rnd.Next(5);
					Console.WriteLine($"So, you're a hunter. let´s go!");
					break;
				default:
					me = new Warriors(nme);
					rndStr = rnd.Next(5);
					rndAgi = rnd.Next(3);
					rndStm = rnd.Next(3);
					rndWdm = rnd.Next(5);
					Console.WriteLine($"you must choose someone!, so you're a warrior");
					break;
			}
			//set stadistics
			stadistic1 = new Stadistics(rndStr, rndAgi, rndStm, rndWdm);
			Console.WriteLine("your base stadistics is:");
			stadistic1.Show();
			me.restartStadistic(stadistic1);
			checkPoint.addStadistic(stadistic1);
			Console.WriteLine();
			me.Show();
			Console.ReadKey();
			//fisrt battle.
			Console.WriteLine($"your first opponent is a goblin");
			IPlayers npc;
			npc = new Warriors("goblin");
			Stadistics ncpStad = new Stadistics(0, 0, 0, 0);
			npc.restartStadistic(ncpStad);
			npc.Show();
			Console.ReadKey();
			//start fight!
			Console.WriteLine("let the fight begins!!!");
			Fights first = new Fights(me, npc);
			first.FightPlayers();
			Console.ReadKey();
			//rest and add new stats
			me.setStadistics();
			stadistic1 = me.getStadistics();
			Console.WriteLine($"Excellent! you have new stadistics:");
			Console.WriteLine("old stadistics:");
			checkPoint[0].Show();
			Console.WriteLine($"new stadistics:");
			stadistic1.Show();
			checkPoint.addStadistic(stadistic1);
			me.rest();
			Console.WriteLine("now, you have these atributes:");
			me.Show();
			Console.ReadKey();
			// new fight
			Console.WriteLine($"your next opponent is a ogre");
			Stadistics ncpStad2 = new Stadistics(2, 3, 3, 2);
			IPlayers npc2 = new Magicians("ogre");
			npc2.restartStadistic(ncpStad2);
			npc2.Show();
			Console.ReadKey();
			//start new battle.
			Console.WriteLine($"let's start the battle:");
			Fights second = new Fights(me, npc2);
			second.FightPlayers();
			Console.ReadKey();
			//rest and add new stats
			me.setStadistics();
			stadistic1 = me.getStadistics();
			Console.WriteLine($"Excellent! you have new stadistics:");
			Console.WriteLine("first stadistics:");
			checkPoint[0].Show();
			Console.WriteLine("second stadistics:");
			checkPoint[1].Show();
			Console.WriteLine("new stadistics:");
			stadistic1.Show();
			checkPoint.addStadistic(stadistic1);
			me.rest();
			Console.WriteLine("now, you have these atributes:");
			me.Show();
			Console.ReadKey();
			// new fight
			Console.WriteLine($"your next opponent is a un-death");
			Stadistics ncpStad3 = new Stadistics(5, 3, 3, 5);
			IPlayers npc3 = new Magicians("un-death");
			npc3.restartStadistic(ncpStad3);
			npc3.Show();
			Console.ReadKey();
			//start new battle.
			Console.WriteLine($"let's start the battle:");
			Fights third = new Fights(me, npc3);
			third.FightPlayers();
			Console.ReadKey();
			Console.WriteLine("Congratulations!!! you Win the game!!!!");
			Console.WriteLine("these are the stadistics that you take in game");
			checkPoint.getCheckpoint().ForEach(x => x.Show());
			Console.ReadKey();
		}
	}
}
