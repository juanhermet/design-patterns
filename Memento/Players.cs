﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Memento
{
	abstract class Players : IPlayers
	{
		protected string name;
		protected int life;
		protected int startLife;
		protected int speed;
		protected int scape;
		protected int attack;
		protected int fights;
		protected Stadistics stadistics;
		public Players(string name)
		{
			this.name = name;
			this.life = 100;
			this.speed = 20;
			this.scape = 10;
			this.attack = 30;
			this.fights = 0;
			startLife = 100;
		}

		//methods
		public void setStadistics()
		{
			Random rnd = new Random();
			Stadistics temp = new Stadistics(rnd.Next(5),rnd.Next(5), rnd.Next(5), rnd.Next(5)); 
			stadistics = stadistics + temp;
			addStadistics();
		}

		public void restartStadistic(Stadistics stadistics)
		{
			this.stadistics = stadistics;
			addStadistics();
		}

		public Stadistics getStadistics()
		{
			return stadistics;
		}

		public string getName()
		{
			return name;
		}

		public int getAttack()
		{
			return attack;
		}
		public int getSpeed()
		{
			return speed;
		}
		public int getLife()
		{
			return life;
		}

		public void addWin()
		{
			fights++;
		}

		public void rest()
		{
			life = startLife;
		}
		//abstract methods
		public abstract void addStadistics();

		public abstract void receiveDamage(int damage);

		public abstract void Show();
		

	}
}
