﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Memento
{
	class CheckPoint
	{
		//this class "CheckPoint" is the caretaker, 
		List<Stadistics> ListStadistics = new List<Stadistics>();
		public void addStadistic(Stadistics stadistics)
		{
			ListStadistics.Add(stadistics);
		}
		public Stadistics this[int index]
		{
			get
			{
				return ListStadistics[index];
			}
		}

		public List<Stadistics> getCheckpoint()
		{
			return ListStadistics;
		}
	}
}
