﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Adapter
{
	class Add : ICalculator // change Roman add to Decimal add and vice versa.
	{
		Romans numR;
		string[] hundreds = { "", "C", "CC", "CCC", "CD", "D", "DC", "DCC", "DCCC", "CM" };
		string[] tens = { "", "X", "XX", "XXX", "XL", "L", "LX", "LXX", "LXXX", "XC" };
		string[] units = { "", "I", "II", "III", "IV", "V", "VI", "VII", "VIII", "IX" };
		//constructors
		public Add(Romans numR)
		{
			this.numR = numR;
		}
		//Interface methods
		public int AddDecimal(int numbee)
		{
			int dec;
			dec = romanToDecimal(numR.getNumber());
			dec += numbee;
			return dec; 
		}
		public int RomToDec()
		{
			return romanToDecimal(numR.getNumber());
		}
		public string DecToRoman(int dec)
		{
			return decimalToRoman(dec);
		}
		public string AddRoman(Romans numbee)
		{
			string add;
			int num, value, total;
			num = romanToDecimal(numbee.getNumber());
			value = romanToDecimal(numR.getNumber());
			total = num + value;
			add = decimalToRoman(total);
			return add;
		}
		private string decimalToRoman(int total)
		{
			string roman;
			int uni;
			int dec;
			int hun;
			if (total < 1000)
			{
				uni = total % 10;
				dec = (total / 10) % 10;
				hun = total / 100;
				roman = hundreds[hun] + tens[dec] + units[uni];
			}
			else
			{
				Console.WriteLine("the Sum upper the 999.");
				roman = "overload";
			}
			return roman;
		}
		//Roman Methods
		private int romanToDecimal(string roman)
		{
			int num = 0;
			for (int i = roman.Length - 1; i >= 0; i--)
			{
				if (i != (roman.Length - 1))
				{
					if (isBiggerThan(roman[i], roman[i + 1]))
					{
						num += convertToNumber(roman[i]);
					}
					else
					{
						num -= convertToNumber(roman[i]);
					}
				}
				else
				{
					num += convertToNumber(roman[i]);
				}
			}
			return num;
		}
		private bool isBiggerThan(char a, char b)
		{
			//M D C L X V I
			if (a == b | a == 'M' | (a == 'D' & (b == 'C' | b == 'L' | b == 'X' | b == 'V')) |
				(a == 'C' & (b == 'X' | b == 'V')) | (a == 'L' & (b == 'X' | b == 'V')) | (a == 'X' & b == 'V') | b == 'I')
			{
				return true;
			}
			else
			{
				return false;
			}
		}
		private int convertToNumber(char a)
		{
			int num;
			if (a == 'I')
			{
				num = 1;
			}
			else
			{
				if (a == 'V')
				{
					num = 5;
				}
				else
				{
					if (a == 'X')
					{
						num = 10;
					}
					else
					{
						if (a == 'L')
						{
							num = 50;
						}
						else
						{
							if (a == 'C')
							{
								num = 100;
							}
							else
							{
								if (a == 'D')
								{
									num = 500;
								}
								else
								{
									if (a == 'M')
									{
										num = 1000;
									}
									else
									{
										num = 0;
									}
								}
							}
						}
					}
				}
			}

			return num;
		}

	}
}
