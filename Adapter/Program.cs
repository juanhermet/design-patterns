﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Adapter
{
	class Program
	{
		static void Main(string[] args)
		{
			Romans primaryRoman = new Romans();
			Romans secondRoman = new Romans();
			int num;
			int number;
			primaryRoman.setNumber();
			//Console.WriteLine("your number in roman is: {0}",primaryRoman.getNumber());
			ICalculator adapter = new Add(primaryRoman);
			Console.WriteLine($"Now we insert a roman number to add:");
			secondRoman.setNumber();
			Console.WriteLine("your add number in Roman number is: {0}", adapter.AddRoman(secondRoman));
			Console.ReadKey();
			Console.WriteLine($"Now we traduce the primary number in decimal:");
			Console.WriteLine("your number in decimal is: {0}", adapter.RomToDec());
			Console.ReadKey();
			Console.WriteLine($"Now we traduce the primary number in decimal:");
			Console.WriteLine($"Insert a decimal number:");
			number = int.Parse(Console.ReadLine());
			num = adapter.AddDecimal(number);
			Console.WriteLine($"");
			Console.WriteLine("your number is: {0}", num);
			Console.WriteLine("your add number in Roman number is: {0}", adapter.DecToRoman(num));
			Console.ReadKey();
		}
	}
}
