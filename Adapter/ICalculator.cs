﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Adapter
{
	interface ICalculator
	{
		int AddDecimal(int number);
		string AddRoman(Romans numbee);
		int RomToDec();
		string DecToRoman(int dec);

	}
}
