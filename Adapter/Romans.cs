﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Adapter
{
	class Romans
	{
		string number;
		string[] hundreds = { "", "C", "CC", "CCC", "CD", "D", "DC", "DCC", "DCCC", "CM" };
		string[] tens = { "", "X", "XX", "XXX", "XL", "L", "LX", "LXX", "LXXX", "XC" };
		string[] units = { "", "I", "II", "III", "IV", "V", "VI", "VII", "VIII", "IX" };
		public Romans()
		{
		}

		public void setNumber()
		{
			string num;
			bool isCorrect;
			do
			{
				Console.WriteLine("insert a Roman Number:");
				num = Console.ReadLine().ToString();
				if (!string.IsNullOrEmpty(num))
				{
					isCorrect = correctRoman(num);
				}
				else
				{
					isCorrect = true;
				}
				} while (isCorrect);
			number = num;
		}
		public string getNumber()
		{
			return number;
		}
		private bool correctRoman(string roman)
		{
			bool isCorrect = false;
			int i;
			if(roman.Length > 3)
			{
				i = roman.Length - 2;
				while (i >= 0)
				{
					if (correctRomanSecuence(roman[i], roman[i + 1]))
					{
						isCorrect = true;
					}
					i--;
				}
			}
			else
			{
				if(roman.Length == 2)
				{
					isCorrect = correctRomanSecuence(roman[0],roman[1]);
				}
				else
				{
					if (roman.Length == 1)
					{
						isCorrect = isRomanNumber(roman[0]);
					}
				}
			}
			
			return isCorrect;
		}
		//M D C L X V I
		private bool correctRomanSecuence(char value,char next)
		{
			bool secuence = false;
			if ((next == 'M' & (value == 'L'| value == 'X' | value == 'V' | value == 'I'))|(next == 'D'&(value=='L'| value == 'V'| value == 'I'))|(next=='L'&(value=='V'|value=='I')))
			{
				secuence = true;
			}
			return secuence;
		}
		private bool isRomanNumber(char value)
		{
			bool isRoman = true;
			if(value != 'I' | value != 'V' | value != 'X' | value != 'L' | value != 'C' | value != 'D' | value != 'M')
			{
				isRoman = false;
			}
			return isRoman;
		}
	}
}
